package com.adailsilva.ngrok.resources;

import java.io.IOException;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adailsilva.ngrok.implementations.NgrokTunnel;
import com.mashape.unirest.http.exceptions.UnirestException;

@RestController
@RequestMapping("/system/ngrok")
public class OnOffNgrokTunnelsResource {

	NgrokTunnel ngrokTunnelByUriAndPort8082 = null;
//	NgrokTunnel ngrokTunnelByUriAndPort8083 = null;
//	NgrokTunnel ngrokTunnelByUriAndPort8084 = null;
//	NgrokTunnel ngrokTunnelByUriAndPort8085 = null;

	@PostMapping("/on")
	public String onNgrok() {

		// Start tunnel, explicit ngrok address:
		try {

//			Runtime.getRuntime().exec("ngrok start --none");
			Process processStart = Runtime.getRuntime().exec("ngrok start --none");
			System.out.println(processStart.toString());

			ngrokTunnelByUriAndPort8082 = new NgrokTunnel("http://127.0.0.1:4040", 8082);
//			ngrokTunnelByUriAndPort8083 = new NgrokTunnel("http://127.0.0.1:4040", 8083);
//			ngrokTunnelByUriAndPort8084 = new NgrokTunnel("http://127.0.0.1:4040", 8084);
//			ngrokTunnelByUriAndPort8085 = new NgrokTunnel("http://127.0.0.1:4040", 8085);
		} catch (IOException | UnirestException e) {
//			e.printStackTrace();
			System.out.println();
			System.out.println("RECONECTANDO...");
			onNgrok();
		}

		// Get and printer the public url:
		System.out.println(ngrokTunnelByUriAndPort8082.url());
//		System.out.println(ngrokTunnelByUriAndPort8083.url());
//		System.out.println(ngrokTunnelByUriAndPort8084.url());
//		System.out.println(ngrokTunnelByUriAndPort8085.url());

		return "Alive ngrok";
	}

	@PostMapping("/off")
	public String offNgrok() {

		// Close the tunnel:
		try {
			ngrokTunnelByUriAndPort8082.close();
//			ngrokTunnelByUriAndPort8083.close();
//			ngrokTunnelByUriAndPort8084.close();
//			ngrokTunnelByUriAndPort8085.close();
			
			Runtime.getRuntime().exec("killall -9 ngrok");
//			Process processStop = Runtime.getRuntime().exec("killall -9 ngrok");
//			System.out.println(processStop);
		} catch (IOException | UnirestException e) {
			e.printStackTrace();
		}

		return "Killed ngrok";
	}

}
