package com.adailsilva.ngrok;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.adailsilva.ngrok.implementations.NgrokTunnel;
import com.mashape.unirest.http.exceptions.UnirestException;

@SpringBootApplication
@SuppressWarnings("unused")
public class NgrokApplication {

	public static void main(String[] args) throws IOException, UnirestException {
		SpringApplication.run(NgrokApplication.class, args);

//		Process processStart = Runtime.getRuntime().exec("ngrok start --none");
//		printResults(processStart);

		// Start tunnel, ngrok address is default:
//		NgrokTunnel ngrokTunnelByPort8082 = new NgrokTunnel(8082);
//		NgrokTunnel ngrokTunnelByPort8083 = new NgrokTunnel(8083);
//		NgrokTunnel ngrokTunnelByPort8084 = new NgrokTunnel(8084);
//		NgrokTunnel ngrokTunnelByPort8085 = new NgrokTunnel(8085);
		
		// Start tunnel, explicit ngrok address:
//		NgrokTunnel ngrokTunnelByUriAndPort8082 = new NgrokTunnel("http://127.0.0.1:4040", 8082);
//		NgrokTunnel ngrokTunnelByUriAndPort8083 = new NgrokTunnel("http://127.0.0.1:4040", 8083);
//		NgrokTunnel ngrokTunnelByUriAndPort8084 = new NgrokTunnel("http://127.0.0.1:4040", 8084);
//		NgrokTunnel ngrokTunnelByUriAndPort8085 = new NgrokTunnel("http://127.0.0.1:4040", 8085);

		// Get and printer the public url:
//		System.out.println(ngrokTunnelByPort8082.url());
//		System.out.println(ngrokTunnelByPort8083.url());
//		System.out.println(ngrokTunnelByPort8084.url());
//		System.out.println(ngrokTunnelByPort8085.url());
		
//		System.out.println(ngrokTunnelByUriAndPort8082.url());
//		System.out.println(ngrokTunnelByUriAndPort8083.url());
//		System.out.println(ngrokTunnelByUriAndPort8084.url());
//		System.out.println(ngrokTunnelByUriAndPort8085.url());

		// Close the tunnel:
//		ngrokTunnelByPort8082.close();
//		ngrokTunnelByPort8083.close();
//		ngrokTunnelByPort8084.close();
//		ngrokTunnelByPort8085.close();
		
//		ngrokTunnelByUriAndPort8082.close();
//		ngrokTunnelByUriAndPort8083.close();
//		ngrokTunnelByUriAndPort8084.close();
//		ngrokTunnelByUriAndPort8085.close();
		
//		Process processStop = Runtime.getRuntime().exec("killall -9 ngrok");
//		printResults(processStop);
	}

	public static void printResults(Process process) throws IOException {
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		String line = "";
		while ((line = bufferedReader.readLine()) != null) {
			System.out.println(line);
		}

	}

}
